import { StyleSheet } from 'react-native'
import { AsyncCalls, Colors } from 'react_native_app/src/commons'

export default StyleSheet.create({

    container: {
        flex: 1,
        backgroundColor: Colors.background,
        paddingBottom: 20,
        paddingTop: 60,

    },

})